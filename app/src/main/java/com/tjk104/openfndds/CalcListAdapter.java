package com.tjk104.openfndds;

import android.content.Context;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class CalcListAdapter extends RecyclerView.Adapter<CalcListAdapter.CalcListViewHolder> {
    private List<CalculationDisplayActivity.CalcItem> mCalcItems;
    private final LayoutInflater mInflater;

    public static class CalcListViewHolder extends RecyclerView.ViewHolder{
        private final TextView calcName;
        private final TextView calcValue;

        public CalcListViewHolder(@NonNull LinearLayout itemView){
            super(itemView);
            calcName = itemView.findViewById(R.id.calc_name);
            calcValue = itemView.findViewById(R.id.calc_value);
        }
        public void setName(String name){
            calcName.setText(name);
        }

        public void setValue(String value){
            calcValue.setText(value);
        }
    }

    CalcListAdapter(Context context){ mInflater = LayoutInflater.from(context); }

    @NonNull
    @Override
    public CalcListViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new CalcListViewHolder((LinearLayout) mInflater.inflate(R.layout.calc_item, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull CalcListViewHolder holder, int position) {
        Resources res = MainActivity.getContext().getResources();
        CalculationDisplayActivity.CalcItem item = mCalcItems.get(position);
        String nameString = res.getString(R.string.nutrient_name, item.getName());
        String amountString = item.getAmount();
        holder.setName(nameString);
        holder.setValue(amountString);
    }

    @Override
    public int getItemCount() {
        return mCalcItems == null ? 0 : mCalcItems.size();
    }

    public void updateList(List<CalculationDisplayActivity.CalcItem> calcItems){
        mCalcItems = calcItems;
        notifyDataSetChanged();
    }
}
