package com.tjk104.openfndds;

import android.content.Context;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.tjk104.openfndds.database.FoodNutrient;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class ComparisonListAdapter extends RecyclerView.Adapter<ComparisonListAdapter.ComparisonListViewHolder> {
    private static class ComparisonEntry{
        private String name;
        private FoodNutrient left;
        private FoodNutrient right;
        public ComparisonEntry(String name){
            this.setName(name);
            this.setLeft(getLeft());
            this.setRight(getRight());
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public FoodNutrient getLeft() {
            return left;
        }

        public void setLeft(FoodNutrient left) {
            this.left = left;
        }

        public FoodNutrient getRight() {
            return right;
        }

        public void setRight(FoodNutrient right) {
            this.right = right;
        }
    }
    private List<ComparisonEntry> mComparisonEntries;
    private final LayoutInflater mInflater;

    public static class ComparisonListViewHolder extends RecyclerView.ViewHolder{
        private final TextView nutrientName;
        private final TextView nutrientAmountLeft;
        private final TextView nutrientAmountRight;

        public ComparisonListViewHolder(@NonNull LinearLayout itemView) {
            super(itemView);
            nutrientName = itemView.findViewById(R.id.comparison_nutrient_name);
            nutrientAmountLeft = itemView.findViewById(R.id.comparison_nutrient_amount_left);
            nutrientAmountRight = itemView.findViewById(R.id.comparison_nutrient_amount_right);
        }

        public void setName(String name){
            nutrientName.setText(name);
        }

        public void setRightAmount(String amount){
            nutrientAmountRight.setText(amount);
        }

        public void setLeftAmount(String amount){
            nutrientAmountLeft.setText(amount);
        }
    }

    ComparisonListAdapter(Context context){ mInflater = LayoutInflater.from(context); }

    @NonNull
    @Override
    public ComparisonListViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ComparisonListViewHolder((LinearLayout) mInflater.inflate(R.layout.comparison_item, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ComparisonListViewHolder holder, int position) {
        Resources res = MainActivity.getContext().getResources();
        ComparisonEntry entry = mComparisonEntries.get(position);
        String name = entry.getName();
        String leftAmount = entry.getLeft() != null ? res.getString(R.string.nutrient_amount, entry.getLeft().amount, entry.getLeft().unitName) : "N/A";
        String rightAmount = entry.getRight() != null ? res.getString(R.string.nutrient_amount, entry.getRight().amount, entry.getRight().unitName) : "N/A";
        holder.setName(name);
        holder.setLeftAmount(leftAmount);
        holder.setRightAmount(rightAmount);
    }

    @Override
    public int getItemCount() {
        return mComparisonEntries == null ? 0 : mComparisonEntries.size();
    }

    public void updateList(List<FoodNutrient> leftFoodNutrients, List<FoodNutrient> rightFoodNutrients){
        mComparisonEntries = new ArrayList<>();
        for(FoodNutrient entry : leftFoodNutrients){
            Optional<ComparisonEntry> match = mComparisonEntries.stream().filter(e -> e.getName().equals(entry.description)).findFirst();
            if(match.isPresent()){
                match.get().setLeft(entry);
            } else {
                ComparisonEntry newEntry = new ComparisonEntry(entry.description);
                newEntry.setLeft(entry);
                mComparisonEntries.add(newEntry);
            }
        }
        for(FoodNutrient entry : rightFoodNutrients){
            Optional<ComparisonEntry> match = mComparisonEntries.stream().filter(e -> e.getName().equals(entry.description)).findFirst();
            if(match.isPresent()){
                match.get().setRight(entry);
            } else {
                ComparisonEntry newEntry = new ComparisonEntry(entry.description);
                newEntry.setRight(entry);
                mComparisonEntries.add(newEntry);
            }
        }
        notifyDataSetChanged();
    }
}
