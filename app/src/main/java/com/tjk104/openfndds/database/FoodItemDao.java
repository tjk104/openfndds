package com.tjk104.openfndds.database;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import java.util.List;

@Dao
public interface FoodItemDao {
    @Query("SELECT * FROM food_items ORDER BY name ASC")
    List<FoodItem> getAll();

    @Query("SELECT * FROM food_items WHERE id = :id")
    FoodItem getById(int id);

// --Commented out by Inspection START (11/30/2020 8:09 AM):
//    @Insert(onConflict = OnConflictStrategy.REPLACE)
//    void insert(FoodItem item);
// --Commented out by Inspection STOP (11/30/2020 8:09 AM)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(List<FoodItem> items);

// --Commented out by Inspection START (11/30/2020 8:09 AM):
//    @Delete
//    void delete(FoodItem item);
// --Commented out by Inspection STOP (11/30/2020 8:09 AM)
}
