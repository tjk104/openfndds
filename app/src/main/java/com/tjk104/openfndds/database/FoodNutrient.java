package com.tjk104.openfndds.database;

import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.Index;
import androidx.room.PrimaryKey;

@Entity(tableName = "food_nutrients",
        foreignKeys = @ForeignKey(entity = FoodItem.class, parentColumns = "id", childColumns = "foodId"),
        indices = @Index("foodId"))
public class FoodNutrient {
    @PrimaryKey
    public final int id;
    public final int foodId;
    public final String description;
    public final float amount;
    public final String unitName;

    public FoodNutrient(int id, int foodId, String description, float amount, String unitName){
        this.id = id;
        this.foodId = foodId;
        this.description = description;
        this.amount = amount;
        this.unitName = unitName;
    }
}
