package com.tjk104.openfndds.database;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity (tableName = "food_items")
public class FoodItem {
    @PrimaryKey
    public final int id;

    @ColumnInfo(name = "name")
    public final String name;

    public FoodItem(int id, String name){
        this.id = id;
        this.name = name;
    }

    @NonNull
    @Override
    public String toString() {
        return this.name;
    }
}
