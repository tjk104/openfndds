package com.tjk104.openfndds;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.tjk104.openfndds.database.FoodDatabase;
import com.tjk104.openfndds.database.FoodItem;
import com.tjk104.openfndds.database.FoodItemDao;
import com.tjk104.openfndds.database.FoodNutrient;
import com.tjk104.openfndds.database.FoodNutrientDao;
import com.tjk104.openfndds.database.FoodPortion;
import com.tjk104.openfndds.database.FoodPortionDao;

import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;

public class FoodDetailActivity extends AppCompatActivity {
    public static final String INCOMING_FOOD_ITEM = "com.tjk104.openfndds.FOOD_ID";

    private FoodItem mFoodItem;
    private List<FoodNutrient> mFoodNutrients;
    private List<FoodPortion> mFoodPortions = new ArrayList<>();

    private NutrientListAdapter mNutrientListAdapter;

    private SharedPreferences mPrefs;
    private Set<Integer> mFavoritesList;

    private static final int NUM_THREADS = 4;
    private final ExecutorService executorService = Executors.newFixedThreadPool(NUM_THREADS);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FoodDatabase db = FoodDatabase.getDatabase(getApplicationContext());
        FoodItemDao mFoodItemDao = db.foodItemDao();
        FoodNutrientDao mFoodNutrientDao = db.foodNutrientDao();
        FoodPortionDao mFoodPortionDao = db.foodPortionDao();
        setContentView(R.layout.activity_food_detail);
        View loadingOverlay = findViewById(R.id.food_detail_loading_overlay);
        loadingOverlay.setVisibility(View.VISIBLE);

        RecyclerView mNutrientRecyclerView = findViewById(R.id.nutrient_recycler_view);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        mNutrientListAdapter = new NutrientListAdapter(this);
        mNutrientRecyclerView.setLayoutManager(mLayoutManager);
        mNutrientRecyclerView.setAdapter(mNutrientListAdapter);

        Spinner mPortionSpinner = findViewById(R.id.food_portion_spinner);
        ArrayAdapter<FoodPortion> mSpinnerAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, mFoodPortions);
        mSpinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mPortionSpinner.setAdapter(mSpinnerAdapter);
        mPortionSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                FoodPortion selected = (FoodPortion) parent.getSelectedItem();
                recalculateNutrientAmounts(selected);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                /*
                 We don't need to do anything when nothing is selected, and in fact
                 doing so should be impossible unless the database is incomplete
                */
            }
        });

        mPrefs = getSharedPreferences(getPackageName(), MODE_PRIVATE);
        mFavoritesList = Arrays.stream(mPrefs.getString("Favorites", "").split(",")).filter(StringUtils::isNumeric).map(Integer::parseInt).collect(Collectors.toSet());

        CheckBox mFavoriteButton = findViewById(R.id.favorite_button);
        mFavoriteButton.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if(isChecked){
                mFavoritesList.add(mFoodItem.id);
                updateFavorites();
                mFavoriteButton.setButtonDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.ic_baseline_star_24));
            } else {
                mFavoritesList.remove(mFoodItem.id);
                updateFavorites();
                mFavoriteButton.setButtonDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.ic_baseline_star_border_24));
            }
        });

        Intent intent = getIntent();
        int id = intent.getIntExtra(INCOMING_FOOD_ITEM, 0);
        executorService.execute(() -> {
            mFoodItem = mFoodItemDao.getById(id);
            mFoodNutrients = mFoodNutrientDao.getFoodNutrients(id);
            mFoodPortions = mFoodPortionDao.getFoodPortions(id);
            mFoodPortions.add(FoodPortion.basePortion);
            runOnUiThread(() -> {
                mNutrientListAdapter.updateList(mFoodNutrients);
                mSpinnerAdapter.clear();
                mSpinnerAdapter.addAll(mFoodPortions);
                ((TextView)findViewById(R.id.food_detail_view_title)).setText(mFoodItem.name);
                ((TextView)findViewById(R.id.food_detail_view_subtitle)).setText(String.format(Locale.ENGLISH, "%d", mFoodItem.id));
                mFavoriteButton.setChecked(mFavoritesList.contains(mFoodItem.id));
                loadingOverlay.setVisibility(View.GONE);
            });
        });
    }

    private void recalculateNutrientAmounts(FoodPortion portion){
        float factor = portion.gramWeight / 100;
        List<FoodNutrient> results = new ArrayList<>();
        for(FoodNutrient entry : mFoodNutrients){
            results.add(new FoodNutrient(entry.id, entry.foodId, entry.description, entry.amount * factor, entry.unitName));
        }
        mNutrientListAdapter.updateList(results);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.detail_menu, menu);

        final MenuItem openDetailView = menu.findItem(R.id.action_open_calc_view);
        openDetailView.setOnMenuItemClickListener(item -> {
            Intent intent = new Intent(this, CalculationDisplayActivity.class);
            intent.putExtra(CalculationDisplayActivity.INCOMING_FOOD_ITEM, mFoodItem.id);
            startActivity(intent);
            return false;
        });
        return super.onCreateOptionsMenu(menu);
    }

    private void updateFavorites(){
        SharedPreferences.Editor edit = mPrefs.edit();
        String favoritesString = mFavoritesList.stream().map(String::valueOf).collect(Collectors.joining(","));
        edit.putString("Favorites", favoritesString);
        edit.apply();
    }
}