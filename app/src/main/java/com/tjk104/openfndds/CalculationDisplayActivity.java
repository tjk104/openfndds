package com.tjk104.openfndds;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.tjk104.openfndds.database.FoodDatabase;
import com.tjk104.openfndds.database.FoodItem;
import com.tjk104.openfndds.database.FoodItemDao;
import com.tjk104.openfndds.database.FoodNutrient;
import com.tjk104.openfndds.database.FoodNutrientDao;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class CalculationDisplayActivity extends AppCompatActivity {
    public static final String INCOMING_FOOD_ITEM = "com.tjk104.openfndds.FOOD_ID";

    public static class CalcItem {
        private String name;
        private String amount;
        public CalcItem(String name, String amount){
            this.setName(name);
            this.setAmount(amount);
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getAmount() {
            return amount;
        }

        public void setAmount(String amount) {
            this.amount = amount;
        }
    }
    private FoodItem mFoodItem;
    private List<FoodNutrient> mFoodNutrients;

    private static final int NUM_THREADS = 4;
    private final ExecutorService executorService = Executors.newFixedThreadPool(NUM_THREADS);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FoodDatabase db = FoodDatabase.getDatabase(getApplicationContext());

        FoodItemDao mFoodItemDao = db.foodItemDao();
        FoodNutrientDao mFoodNutrientDao = db.foodNutrientDao();
        setContentView(R.layout.activity_calculation_display);
        View loadingOverlay = findViewById(R.id.calculation_view_loading_overlay);
        loadingOverlay.setVisibility(View.VISIBLE);

        RecyclerView mCalcRecyclerView = findViewById(R.id.calculation_recycler_view);
        CalcListAdapter mCalcListAdapter = new CalcListAdapter(this);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        mCalcRecyclerView.setLayoutManager(mLayoutManager);
        mCalcRecyclerView.setAdapter(mCalcListAdapter);

        Intent intent = getIntent();
        int id = intent.getIntExtra(INCOMING_FOOD_ITEM, 0);
        executorService.execute(() -> {
            mFoodItem = mFoodItemDao.getById(id);
            mFoodNutrients = mFoodNutrientDao.getFoodNutrients(id);
            ((TextView)findViewById(R.id.calculation_view_title)).setText(mFoodItem.name);
            ((TextView)findViewById(R.id.calculation_view_subtitle)).setText(String.format(Locale.ENGLISH,"%d", mFoodItem.id));
            mCalcListAdapter.updateList(runCalculations());
            loadingOverlay.setVisibility(View.GONE);
        });

    }

    private List<CalcItem> runCalculations(){
        List<CalcItem> results = new ArrayList<>();
        Optional<FoodNutrient> totalCarbsMatch = mFoodNutrients.stream().filter(e -> e.description.contains("Carbohydrate")).findFirst();
        Optional<FoodNutrient> totalFatMatch = mFoodNutrients.stream().filter(e -> e.description.contains("Total lipid")).findFirst();
        Optional<FoodNutrient> totalProteinMatch = mFoodNutrients.stream().filter(e -> e.description.contains("Protein")).findFirst();
        Optional<FoodNutrient> fiberMatch = mFoodNutrients.stream().filter(e -> e.description.contains("Fiber, total dietary")).findFirst();
        if(totalCarbsMatch.isPresent() && fiberMatch.isPresent()){
            float totalCarbs = totalCarbsMatch.get().amount;
            float fiber = fiberMatch.get().amount;
            float netCarbs = totalCarbs - fiber;
            CalcItem netCarbItem = new CalcItem("Net Carbs (Per 100g)", String.format(Locale.ENGLISH, "%1$.2f", netCarbs));
            results.add(netCarbItem);
        }
        if(totalProteinMatch.isPresent() && totalCarbsMatch.isPresent() && totalFatMatch.isPresent()){
            float totalProtein = totalProteinMatch.get().amount;
            float totalCarbs = totalCarbsMatch.get().amount;
            if (fiberMatch.isPresent()) {
                totalCarbs -= fiberMatch.get().amount;
            }
            float totalFat = totalFatMatch.get().amount;
            float totalMacro = totalProtein + totalCarbs + totalFat;
            results.add(new CalcItem("Protein Percentage", String.format(Locale.ENGLISH, "%1$.2f%%", (totalProtein / totalMacro) * 100)));
            results.add(new CalcItem("Carbohydrate Percentage", String.format(Locale.ENGLISH, "%1$.2f%%", (totalCarbs / totalMacro) * 100)));
            results.add(new CalcItem("Fat Percentage", String.format(Locale.ENGLISH, "%1$.2f%%", (totalFat / totalMacro) * 100)));
        }
        Optional<FoodNutrient> calMatch = mFoodNutrients.stream().filter(e -> e.description.equals("Energy")).findFirst();
        if(calMatch.isPresent()){
            float cals = calMatch.get().amount;
            for(FoodNutrient nutrient : mFoodNutrients){
                if (!nutrient.description.equals("Energy")) {
                    String calcName = nutrient.description + " Density";
                    float calcAmount = nutrient.amount/cals;
                    CalcItem newCalc = new CalcItem(calcName, String.format(Locale.ENGLISH, "%1$.2f", calcAmount));
                    results.add(newCalc);
                }
            }
        }

        return results;
    }
}