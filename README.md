# Open FNDDS Viewer
This project is designed to take the data from the USDA's Food and Nutrition Database for Dietary Studies (available [here](https://fdc.nal.usda.gov/download-datasets.html)) and present it in a way that people can easily access, since right now the way that it is structured and formatted makes getting any information an extremely tedious and time-consuming task.

---
## Usage Tips

On first startup, the app will run through the included csv files to parse all available
    information into the internal database, this will take a while but since you\'ve gotten to this screen you probably already know that.
<br><br>
This should only have to happen once, however if something goes wrong (i.e. the app is killed
    before it is finished processing) you will need to go to the app settings and clear all storage, which will cause the database to be rebuilt.
<br><br>
The search functionality on the main screen is pretty simple, it just matches everything that
    contains each word that you type. While it is case-insensitive, it doesn\'t accommodate for
    spelling errors or anything like that, e.g. "Apple" and "apple" will match the same
    things but if you type "aple" etc. it likely won\'t match anything.
<br><br>
Also, it should be noted that this database is pretty extensive and detailed, and as such you\'ll have
    better luck if you\'re more specific when searching, e.g. searching for "raw apple" instead
    of just "apple", since "apple" will match anything that contains the word "apple" anywhere
    in the food name, which is a lot of them, and raw apples aren\'t even on the first screen
    of results.
<br><br>
To compare two items, select "Compare Items" from the kebab menu, then search for the food
    items that you want on each side, the list of nutrients will update automatically. Since
    there\'s no guarantee that the two items will have the same portion sizes (and because
    the comparison screen is fairly cluttered as is) there\'s no option to change the portions
    when comparing items, it just displays the comparison as amount per 100g.
<br><br>
From the detail view of a food item, you can select "View Extra Values" from the kebab
    menu to view some extra calculations based on the item\'s nutrient values, which currently
    consists of net carbs and nutrient densities for each nutrient.
<br><br>
By clicking the star button on the detail view, you can mark an item as a "favorite" which
    can then be filtered on the main list screen by clicking the star in the menu bar.
<br><br>
The database frequently uses the acronyms "NS" and "NFS" in the food names, these stand for
    "Not Specified" and "Not Further Specified" respectively. This is used when some factor
    regarding the specific food item isn\'t known, such as cooking method or any other descriptor.
    In many cases there are several entries for different preparations of the same food along
    with one that has one of those acronyms, e.g. for "almonds" there are entries for
    "Almonds, salted", "Almonds, honey roasted", and others, including "Almonds, NFS." Always
    check for a more specific match for what you\'re looking for, but if it isn\'t there the
    an NS/NFS entry will serve as a good estimate, although due to its nature of having
    an unknown it will generally not be as accurate as the more specific entries.

---
## License Info

This project currently depends on the AndroidX, Google Material, and OpenCSV libraries,
    all of which are licenced under The Apache Software License, Version 2.0, and since I
    am not familiar enough with how software licenses work to choose a different one
    this project itself is under the Apache 2.0 license as well.<br>
The full text of the license can be found at https://www.apache.org/licenses/LICENSE-2.0