This project is designed to take the data from the USDA's Food and Nutrition Database for Dietary Studies (available here: (https://fdc.nal.usda.gov/download-datasets.html) and present it in a way that people can easily access, since right now the way that it is structured and formatted makes getting any information an extremely tedious and time-consuming task.

In addition to displaying the data, it also includes some more handy features including:
* Searching for food items
* Adding foods to a list of favorites for quick access
* Comparing information about different food items
* Showing more detailed info about nutrient densities, macronutrient percentages, etc.
* Fully offline and privacy-friendly, no permissions needed whatsoever!